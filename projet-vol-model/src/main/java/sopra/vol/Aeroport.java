package sopra.vol;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.validation.constraints.NotEmpty;

@Entity
public class Aeroport {
	@Id
	@NotEmpty(message = "Le code aéroport est obligatoire")
	private String code;
	@ManyToMany
	@JoinTable(
	name="aeroport_ville",
	uniqueConstraints=@UniqueConstraint(columnNames = { "AEROPORT_CODE", "VILLE_ID" }),
	joinColumns=@JoinColumn(name="AEROPORT_CODE"),
	inverseJoinColumns=@JoinColumn(name="VILLE_ID"))
	private List<Ville> villes = new ArrayList<Ville>();
	@Version
	private int version;

	public Aeroport() {
		super();
	}

	public Aeroport(String code, int version) {
		super();
		this.code = code;
		this.version = version;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public List<Ville> getVilles() {
		return villes;
	}

	public void setVilles(List<Ville> villes) {
		this.villes = villes;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

}
