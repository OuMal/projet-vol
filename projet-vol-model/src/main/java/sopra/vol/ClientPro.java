package sopra.vol;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
public class ClientPro extends Client {
	@Column(length = 100)
	@NotEmpty(message = "Numero de Siret obligatoire !")
	private String numeroSiret;
	@Column(length = 100)
	@NotEmpty(message = "Nom de l'entreprise obligatoire !")
	@Size(min = 2, message = "Le nom doit comporter au moins 2 caractères !")
	@Pattern(regexp = "^[A-Z].*", message = "Le nom doit commencer par une majuscule !")
	private String nomEntreprise;
	@Column(length = 100)
	private String numTVA;
	@Enumerated(EnumType.STRING)
	@Column(length = 100)
	@NotNull(message = "Type entreprise obligatoire !")
	private TypeEntreprise typeEntreprise;
	private Adresse facturation;

	public ClientPro() {
		super();
	}

	public ClientPro(Long id, String mail, String telephone, MoyenPaiement moyenPaiement, String numeroSiret,
			String nomEntreprise, String numTVA, TypeEntreprise typeEntreprise) {
		super(id, mail, telephone, moyenPaiement);
		this.numeroSiret = numeroSiret;
		this.nomEntreprise = nomEntreprise;
		this.numTVA = numTVA;
		this.typeEntreprise = typeEntreprise;
	}

	public ClientPro(@NotEmpty(message = "Numero de Siret obligatoire !") String numeroSiret,
			@NotEmpty(message = "Nom de l'entreprise obligatoire !") @Size(min = 2, message = "Le nom doit comporter au moins 2 caractères !") @Pattern(regexp = "^[A-Z].*", message = "Le nom doit commencer par une majuscule !") String nomEntreprise,
			String numTVA, @NotNull(message = "Type entreprise obligatoire !") TypeEntreprise typeEntreprise,
			Adresse facturation) {
		super();
		this.numeroSiret = numeroSiret;
		this.nomEntreprise = nomEntreprise;
		this.numTVA = numTVA;
		this.typeEntreprise = typeEntreprise;
		this.facturation = facturation;
	}

	public String getNumeroSiret() {
		return numeroSiret;
	}

	public void setNumeroSiret(String numeroSiret) {
		this.numeroSiret = numeroSiret;
	}

	public String getNomEntreprise() {
		return nomEntreprise;
	}

	public void setNomEntreprise(String nomEntreprise) {
		this.nomEntreprise = nomEntreprise;
	}

	public String getNumTVA() {
		return numTVA;
	}

	public void setNumTVA(String numTVA) {
		this.numTVA = numTVA;
	}

	public TypeEntreprise getTypeEntreprise() {
		return typeEntreprise;
	}

	public void setTypeEntreprise(TypeEntreprise typeEntreprise) {
		this.typeEntreprise = typeEntreprise;
	}

	public Adresse getFacturation() {
		return facturation;
	}

	public void setFacturation(Adresse facturation) {
		this.facturation = facturation;
	}

}
