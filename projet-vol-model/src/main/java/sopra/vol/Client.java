package sopra.vol;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Version;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity

public abstract class Client {

	@Id
	@GeneratedValue
	private Long id;
	@Column(length = 100)
	@Email
	@NotEmpty(message = "Mail obligatoire !")
	private String mail;
	@Column(length = 100)
	@NotEmpty(message = "Telephone obligatoire !")
	private String telephone;
	@Version
	private int version;
	@Column(length = 100)
	@Enumerated(EnumType.STRING)
	@NotNull(message = "Moyen de paiement obligatoire !")
	private MoyenPaiement moyenPaiement;
	@Embedded
	@Column(length = 100)
	private Adresse principale;
	@Embedded
	@Column(length = 100)
	@AttributeOverrides({
			@AttributeOverride(name = "voie", column = @Column(name = "voie_facturation")),
			@AttributeOverride(name = "complement", column = @Column(name = "complement_facturation")),
			@AttributeOverride(name = "codePostal", column = @Column(name = "codePostal_facturation")),
			@AttributeOverride(name = "ville", column = @Column(name = "ville_facturation")),
			@AttributeOverride(name = "pays", column = @Column(name = "pays_facturation")) })
	private Adresse facturation;
	@OneToMany(mappedBy = "client", fetch = FetchType.LAZY)
	private List<Reservation> reservations = new ArrayList<Reservation>();

	public Client() {
		super();
	}

	public Client(Long id, String mail, String telephone, MoyenPaiement moyenPaiement) {
		super();
		this.id = id;
		this.mail = mail;
		this.telephone = telephone;
		this.moyenPaiement = moyenPaiement;
	}
	

	public Client(Long id, String mail, String telephone, int version, MoyenPaiement moyenPaiement, Adresse principale,
			Adresse facturation) {
		super();
		this.id = id;
		this.mail = mail;
		this.telephone = telephone;
		this.version = version;
		this.moyenPaiement = moyenPaiement;
		this.principale = principale;
		this.facturation = facturation;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public MoyenPaiement getMoyenPaiement() {
		return moyenPaiement;
	}

	public void setMoyenPaiement(MoyenPaiement moyenPaiement) {
		this.moyenPaiement = moyenPaiement;
	}

	public Adresse getPrincipale() {
		return principale;
	}

	public void setPrincipale(Adresse principale) {
		this.principale = principale;
	}

	public Adresse getFacturation() {
		return facturation;
	}

	public void setFacturation(Adresse facturation) {
		this.facturation = facturation;
	}

	public List<Reservation> getReservations() {
		return reservations;
	}

	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

}
