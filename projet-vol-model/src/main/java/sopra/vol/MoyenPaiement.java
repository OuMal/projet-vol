package sopra.vol;

public enum MoyenPaiement {
	CB("moyenPaiement.CB"),CHEQUE("moyenPaiement.CHEQUE"),ESPECE("moyenPaiement.ESPECE"),VIREMENT("moyenPaiement.VIREMENT");
	
	private final String label;

	private MoyenPaiement(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
}
