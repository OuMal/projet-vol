package sopra.vol;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
public class ClientParticulier extends Client {
	@Enumerated(EnumType.STRING)
	@Column(length = 100)
	@NotNull(message = "Civilité obligatoire !")
	private Civilite civilite;
	@Column(length = 100)
	@NotEmpty(message = "Nom obligatoire !")
	@Size(min = 2, message = "Le nom doit comporter au moins 2 caractères !")
	@Pattern(regexp = "^[A-Z].*", message = "Le nom doit commencer par une majuscule !")
	private String nom;
	@Column(length = 100)
	@NotEmpty(message = "Prenom obligatoire !")
	@Size(min = 2, message = "Le prénom doit comporter au moins 2 caractères !")
	@Pattern(regexp = "^[A-Z].*", message = "Le prénom doit commencer par une majuscule !")
	private String prenom;
	private Adresse principale;

	public ClientParticulier() {
		super();
	}

	public ClientParticulier(Long id, String mail, String telephone, MoyenPaiement moyenPaiement, Civilite civilite,
			String nom, String prenom) {
		super(id, mail, telephone, moyenPaiement);
		this.civilite = civilite;
		this.nom = nom;
		this.prenom = prenom;
	}
	
	

	public ClientParticulier(@NotNull(message = "Civilité obligatoire !") Civilite civilite,
			@NotEmpty(message = "Nom obligatoire !") @Size(min = 2, message = "Le nom doit comporter au moins 2 caractères !") @Pattern(regexp = "^[A-Z].*", message = "Le nom doit commencer par une majuscule !") String nom,
			@NotEmpty(message = "Prenom obligatoire !") @Size(min = 2, message = "Le prénom doit comporter au moins 2 caractères !") @Pattern(regexp = "^[A-Z].*", message = "Le prénom doit commencer par une majuscule !") String prenom,
			Adresse principale) {
		super();
		this.civilite = civilite;
		this.nom = nom;
		this.prenom = prenom;
		this.principale = principale;
	}

	public Civilite getCivilite() {
		return civilite;
	}

	public void setCivilite(Civilite civilite) {
		this.civilite = civilite;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Adresse getPrincipale() {
		return principale;
	}

	public void setPrincipale(Adresse principale) {
		this.principale = principale;
	}

}
