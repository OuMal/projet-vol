package sopra.vol;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Version;
import javax.validation.constraints.NotEmpty;


@Entity
public class Compagnie {
	@Id
	@GeneratedValue
	private Long id;
	@Column(length = 100)
	@NotEmpty(message = "Le nom de la compagnie est obligatoire")
	private String nomCompagnie;
	@OneToMany(mappedBy="compagnie", fetch = FetchType.EAGER)
	private List<Vol> vols = new ArrayList<Vol>();
	@Version
	private int version;
	
	public Compagnie() {
		super();
	}

	public Compagnie(Long id, String nomCompagnie, int version) {
		super();
		this.id = id;
		this.nomCompagnie = nomCompagnie;
		this.version = version;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNomCompagnie() {
		return nomCompagnie;
	}

	public void setNomCompagnie(String nomCompagnie) {
		this.nomCompagnie = nomCompagnie;
	}

	public List<Vol> getVols() {
		return vols;
	}

	public void setVols(List<Vol> vols) {
		this.vols = vols;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

}
