package sopra.vol;

public enum TypePieceIdentite {
	CARTE_IDENTITE("TypePieceIdentite.carteIdentite"), PASSEPORT ("TypePieceIdentite.passeport");
	private final String label;
	
	private TypePieceIdentite(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
	
}
