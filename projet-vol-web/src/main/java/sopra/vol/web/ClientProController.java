package sopra.vol.web;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import sopra.vol.Adresse;
import sopra.vol.Civilite;
import sopra.vol.Client;
import sopra.vol.ClientParticulier;
import sopra.vol.ClientPro;
import sopra.vol.MoyenPaiement;
import sopra.vol.TypeEntreprise;
import sopra.vol.dao.IClientRepository;


@Controller
@RequestMapping("/clientPro")
public class ClientProController {
	@Autowired
	private IClientRepository clientRepo;
//	private Client clientPro = (Client) new ClientPro();
//	private Client clientParticulier = (Client) new ClientParticulier();

	public ClientProController() {
		super();

	}

	@GetMapping({ "", "/list" })
	public String list(Model model) {
		model.addAttribute("mesClients", clientRepo.findAll());

		return "clientPro/list";
	}

	@GetMapping("/addPro")
	public String addPro(Model model) {
		model.addAttribute("monClient", new ClientPro());
		model.addAttribute("moyenpaiements", MoyenPaiement.values());
		model.addAttribute("typeentreprises", TypeEntreprise.values());
		model.addAttribute("facturation", new Adresse());
		return "clientPro/formPro";
	}

	@GetMapping("/editPro")
	public String editPro(@RequestParam Long id, Model model) {
		Optional<Client> optClient = clientRepo.findById(id);

		if (optClient.isPresent()) {
			model.addAttribute("monClient", optClient.get());
			model.addAttribute("principale", optClient.get().getPrincipale());
		}

		model.addAttribute("moyenpaiements", MoyenPaiement.values());
		model.addAttribute("typeentreprises", TypeEntreprise.values());
		
		return "clientPro/formPro";
	}
	
	@PostMapping("/savePro")
	public String savePro(@ModelAttribute("monClient") @Valid ClientPro clientPro, BindingResult result, Model model) {
		//new EleveValidator().validate(eleve, result);

		if (result.hasErrors()) {
			model.addAttribute("moyenpaiements", MoyenPaiement.values());
			model.addAttribute("typeentreprises", TypeEntreprise.values());

			return "clientPro/formPro";
		}

		clientRepo.save(clientPro);

		return "redirect:list";
	}
	
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable Long id) {

		clientRepo.deleteById(id);

		return "redirect:/clientPro/list";
	}

	@GetMapping("/cancel")
	public String cancel(Model model) {
		return "forward:list";
	}

}
