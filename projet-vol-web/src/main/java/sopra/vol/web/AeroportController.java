package sopra.vol.web;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import sopra.vol.Aeroport;
import sopra.vol.Civilite;
import sopra.vol.dao.IAeroportRepository;


@Controller
@RequestMapping("/aeroport")
public class AeroportController {
	@Autowired
	private IAeroportRepository aeroportRepo;

	public AeroportController() {
		super();

	}

	@GetMapping({ "", "/list" })
	public String list(Model model) {
		model.addAttribute("mesAeroports", aeroportRepo.findAll());

		return "aeroport/list";
	}

	@GetMapping("/add")
	public String add(Model model) {
		model.addAttribute("monAeroport", new Aeroport());

		return "aeroport/form";
	}

	@GetMapping("/edit")
	public String edit(@RequestParam String id, Model model) {
		Optional<Aeroport> optAeroport = aeroportRepo.findById(id);

		if (optAeroport.isPresent()) {
			model.addAttribute("monAeroport", optAeroport.get());
		}

		return "aeroport/form";
	}

	@PostMapping("/save")
	public String save(@ModelAttribute("monAeroport") @Valid Aeroport aeroport, BindingResult result, Model model) {
		new AeroportValidator().validate(aeroport, result);

		if (result.hasErrors()) {

			return "aeroport/form";
		}

		aeroportRepo.save(aeroport);

		return "redirect:list";
	}

	@GetMapping("/delete/{id}")
	public String delete(@PathVariable String id) {

		aeroportRepo.deleteById(id);

		return "redirect:/aeroport/list";
	}

	@GetMapping("/cancel")
	public String cancel(Model model) {
		return "forward:list";
	}

}
