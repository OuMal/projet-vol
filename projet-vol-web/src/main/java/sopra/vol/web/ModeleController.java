package sopra.vol.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
@RequestMapping("/maPage")
public class ModeleController {
//	@Autowired
//	private IEleveRepository eleveRepo;

	public ModeleController() {
		super();

	}

	@GetMapping({ "", "/list" })
	public String list(Model model) {
//		model.addAttribute("mesEleves", eleveRepo.findAll());

		return "maPage/list";
	}

	@GetMapping("/add")
	public String add(Model model) {
//		model.addAttribute("monEleve", new Eleve());
//		model.addAttribute("civilites", Civilite.values());

		return "maPage/form";
	}

	@GetMapping("/edit")
	public String edit(@RequestParam Long id, Model model) {
//		Optional<Eleve> optEleve = eleveRepo.findById(id);
//
//		if (optEleve.isPresent()) {
//			model.addAttribute("monEleve", optEleve.get());
//		}
//
//		model.addAttribute("civilites", Civilite.values());

		return "maPage/form";
	}

	@PostMapping("/save")
//	public String save(@ModelAttribute("monEleve") @Valid Eleve eleve, BindingResult result, Model model) {
//		new EleveValidator().validate(eleve, result);
//
//		if (result.hasErrors()) {
//			model.addAttribute("civilites", Civilite.values());
//
//			return "maPage/form";
//		}
//
//		eleveRepo.save(eleve);
//
//		return "redirect:list";
//	}

	@GetMapping("/delete/{id}")
	public String delete(@PathVariable Long id) {

//		eleveRepo.deleteById(id);

		return "redirect:/maPage/list";
	}

	@GetMapping("/cancel")
	public String cancel(Model model) {
		return "forward:list";
	}

}
