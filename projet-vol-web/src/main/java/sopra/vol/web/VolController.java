package sopra.vol.web;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import sopra.vol.Vol;
import sopra.vol.dao.IAeroportRepository;
import sopra.vol.dao.ICompagnieRepository;
import sopra.vol.dao.IVolRepository;

@Controller
@RequestMapping("/vol")
public class VolController {
	@Autowired
	private IVolRepository volRepo;
	@Autowired
	private ICompagnieRepository compagnieRepo;
	@Autowired
	private IAeroportRepository aeroportRepo;
	
	public VolController() {
		super();

	}

	@GetMapping({ "", "/list" })
	public String list(Model model) {
		model.addAttribute("mesVols", volRepo.findAll());

		return "vol/list";
	}

	@GetMapping("/add")
	public String add(Model model) {
		model.addAttribute("monVol", new Vol());
		model.addAttribute("compagnies", compagnieRepo.findAll());
		model.addAttribute("aeroports", aeroportRepo.findAll());

		return "vol/form";
	}

	@GetMapping("/edit")
	public String edit(@RequestParam Long id, Model model) {
		Optional<Vol> optVol = volRepo.findById(id);

		if (optVol.isPresent()) {
			model.addAttribute("monVol", optVol.get());
			model.addAttribute("compagnies", compagnieRepo.findAll());
			model.addAttribute("aeroports", aeroportRepo.findAll());
		}

		return "vol/form";
	}

	@PostMapping("/save")
	public String save(@ModelAttribute("monVol") @Valid Vol vol, BindingResult result, Model model) {

		if (result.hasErrors()) {
			model.addAttribute("compagnies", compagnieRepo.findAll());
			model.addAttribute("aeroports", aeroportRepo.findAll());
			return "vol/form";
		}

		volRepo.save(vol);

		return "redirect:list";
	}

	@GetMapping("/delete/{id}")
	public String delete(@PathVariable Long id) {

		volRepo.deleteById(id);

		return "redirect:/vol/list";
	}

	@GetMapping("/cancel")
	public String cancel(Model model) {
		return "forward:list";
	}

}
