package sopra.vol.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import sopra.vol.Passager;

public interface IPassagerRepository extends JpaRepository<Passager, Long> {
	List<Passager> findByNom(String nom);
	List<Passager> findByPrenom(String prenom);
}

