package sopra.vol.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import sopra.vol.VoyageVol;

public interface IVoyageVolRepository extends JpaRepository<VoyageVol, Long> {
}
