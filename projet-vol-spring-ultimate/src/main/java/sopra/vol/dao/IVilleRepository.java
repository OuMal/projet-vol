package sopra.vol.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import sopra.vol.Ville;

public interface IVilleRepository extends JpaRepository<Ville, Long> {
}
